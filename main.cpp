#include "mbed.h"
#include "PinNames.h"
#include "serial_api.h"
#include "sleep_api.h"
#include <PeripheralNames.h>
#include <cstring>

#define HEADER      0x21
#define COLUMN      0x01
#define CABINET     0x01
#define DOOR_CLOSE  0x00
#define DOOR_OPEN   0x01
#define CS          0x00
#define SYN         0x16

// IRQ callback definition
void IRQCallback();

ssize_t total_read = 0;
bool readyToRead = false;

//----- OPEN THE UART2 -----
UnbufferedSerial uart2(PA_2, PA_3); // TX, RX

// main() runs in its own thread in the OS
int main()
{
    char szCommand[256];
    int total = 8;  // size of respond

    //reset szCommand buffer
    memset(szCommand, 0, 256);
    
    // check UART2 is readable and writable
    if( !uart2.writable() )
        return -1;
    if( !uart2.readable() )
        return -1;

    //----- SEND Command 1 -----
    sprintf(szCommand, "%c%c%c%c%c%c%c%c",
        HEADER, CABINET, COLUMN, 1, 1, DOOR_OPEN, CS, SYN);

    uart2.write(szCommand, 8);

    // REGISTER IRQ CALLBACK FOR RECEIVING
    // by documentation the run sleeps until receive
    uart2.attach(IRQCallback, UnbufferedSerial::RxIrq);

    //----- SLEEP MODE -----
    while(!readyToRead)
        hal_sleep();

    //----- RECEIVE ANSWER Command 1 -----
    while( total_read >= total )
    {
        total_read += uart2.read(szCommand + total_read, 1);
    }
    szCommand[total_read] = 0;

    //----- SEND Command 7 -----
    total = 8;  // size of respond
    sprintf(szCommand, "%c%c%c%c%c%c%c%c",
        HEADER, CABINET, 0, 7, 1, DOOR_OPEN, CS, SYN);

    uart2.write(szCommand, 8);

    // REGISTER IRQ CALLBACK FOR RECEIVING
    // by documentation the run sleeps until receive
    readyToRead = false;
    uart2.attach(IRQCallback, UnbufferedSerial::RxIrq);

    //----- SLEEP MODE -----
    while(!readyToRead)
        hal_sleep();

    //----- RECEIVE ANSWER Command 7 -----
    while( total_read >= total )
    {
        total_read += uart2.read(szCommand + total_read, 1);
    }
    szCommand[total_read] = 0;

    // CLOSE UART2
    uart2.close();
}

void IRQCallback()
{
    // DETTACH IRQ CALLBACK FOR RECEIVE IRQ
    uart2.attach(NULL, UnbufferedSerial::RxIrq);

    readyToRead = true;
}
